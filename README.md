- 👋 Hello, I'm @baasankhuu.p
- 👀 I am interested in becoming a mern stack
- 🌱 I am currently studying at the National University of Applied Sciences
- 💞️ I want to work in a software company
- 🧑‍🎓National University of Applied Sciences.

<h1 align="center">⚡About me⚡</h1>
<p align="center">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=azure,react,nextjs,html,tailwind,nodejs,javascript,typescript,expressjs,github,vercel,mongodb,postgresql,postman,cpp,cs,php" />
  </a>
</p>
<h4>  Hello, good day to all of you who are reading my CV. My name is Baasankhu with Purev surname. Born in Mongolia. In 2019-2023, I graduated from the National University  of Mongolia. University majoring in "Software".
</h4>
<h5>
"Software"
The following courses were evaluated within the content course./0-100/
  <ul>
    <li>Algorithm base -> 93%</li>
    <li>Data structure /C/ >95%</li>
    <li>Windows Programming /C#/ -> 95%</li>
    <li>Web Programming /HTML, CSS, Javascript, NodeJS/ -> 90%</li>
    <li>Java technology/Java/ ->100%</li>
    <li>Visual Programming/JavaFX/->100%</li>
    <li>C programming language /C, C++/ ->83%</li>
    <li>Database Programming /MSSQL, Postresql, SQL/ ->93%</li>
  </ul>

Back end:
  - C# https://dev.azure.com/19B1NUM1461
  - NodeJs Express

Database
<ol>
  <li>Mongodb</li>
  <li>Postgresql</li>
  <li>Mssql</li>
  <li>Mysql</li>
</ol>

Front end: /html,css,javascript .etc/
<ol>
  <li>ReactJs/Reac Native</li>
  <li>NextJS</li>
  <li>Tailwind</li>
  <li>Php</li>
</ol>

E-LEARNING
  Basic sites for that language. Documents are often read
  Challenge Test-> https://www.w3profile.com/baaskaa_software_engineer
  Document writing and 1234.mn
</h5>
  
## 💖 Support the Project

<!-- Thank you so much already for using my projects! If you want to go a step further and support my open source work, buy me a coffee:

<a href='https://ko-fi.com/Q5Q860KQ2' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi1.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

To support the project directly, feel free to open issues for icon suggestions, or contribute with a pull request! -->
## My Development Projects

- https://aws-omega.vercel.app/
- https://2023.awsmongolia.com/
- https://srve-ashen.vercel.app/active/cljj9ybpi006inu08pfrm6mvi
- https://baasankhuu-p.github.io/MyProject/MyProjectHtml/
